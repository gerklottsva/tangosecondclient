#include <iostream>
#include <tango.h>

class AttrDblReadCb : public Tango::CallBack
{
	void read_attr(Tango::AttrReadEvent *ev)
	{
		if (ev->err)
		{
			std::cerr << ev->device->name() << " asynch read failed" << endl;
		}
		else
		{
			for (int i = 0; i < ev->attr_names.size(); ++i)
			{
				double value;
				ev->argout->at(i) >> value;
				cout << ev->attr_names.at(i) << " was asynch read: " << value << endl;
			}
		}
	}
};

int main()
{
	std::string devName = "sys/tangotest/3";

	try
	{
		Tango::DeviceProxy device(devName);
		
		cout << "Ping sys/tangotest/3: " << device.ping() << " ms" << endl;
	}
	catch (Tango::DevFailed &e)
	{
		cout << e.errors[0].desc << endl;
		return 1;
	}

	cout << "Tango Second Client" << endl;

	return 0;
}